import 'package:flutter/material.dart';
import 'package:login_bloc_pattern_app/bloc.dart';
import 'package:login_bloc_pattern_app/pagina_dos.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: LoginPage(),
      ),
    );
  }
}

class LoginPage extends StatelessWidget {

  redirigePage(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => PaginaDos(),
    ));
  }

  @override
  Widget build(BuildContext context) {

    final bloc = Bloc();

    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder<String>(
              stream: bloc.email,
              builder: (context, snapshot) => TextField(
                onChanged: (s) => bloc.emailChanged.add(s),
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Escribe tu Email",
                  labelText: "Email",
                  errorText: snapshot.error,
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            StreamBuilder<String>(
              stream: bloc.password,
              builder: (context, snapshot) => TextField(
                onChanged: (s) => bloc.passwordChanged.add(s),
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Escribe tu Password",
                  labelText: "Password",
                  errorText: snapshot.error,
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            StreamBuilder<bool>(
              stream: bloc.submitCheck,
              builder: (context, snapshot) => RaisedButton(
                color: Colors.tealAccent,
                onPressed: snapshot.hasData 
                           ? () => redirigePage(context) 
                           : null,
                child: Text("Submit"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}