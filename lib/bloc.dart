import 'dart:async';
import 'package:login_bloc_pattern_app/validator.dart';
import 'package:rxdart/rxdart.dart';

/// Implemento BaseBloc pero lo extiendo de Object y agrego los Validators
class Bloc extends Object with Validators implements BaseBloc {

	//final _emailController = StreamController<String>();
	//final _passwordController = StreamController<String>();
	final _emailController = BehaviorSubject<String>();
	final _passwordController = BehaviorSubject<String>();

	//Function(String) get emailChanged => _emailController.sink.add;
	//Function(String) get passwordChanged => _passwordController.sink.add;

	StreamSink<String> get emailChanged => _emailController.sink;
	StreamSink<String> get passwordChanged => _passwordController.sink;


	Stream<String> get email => _emailController.stream.transform(emailValidator);
	Stream<String> get password => _passwordController.stream.transform(passwordValidator);

	// aqui se usa rxdart que es una libreria ReactivX
	Stream<bool> get submitCheck => Observable.combineLatest2(email, password, (e,p) => true);


	submit() {
		print("Email y pasword introducidos");
	}

	@override
	void dispose() {
		_emailController?.close();
		_passwordController?.close();
	}

}

abstract class BaseBloc {
	void dispose();
}