import 'dart:async';

mixin Validators {

	var emailValidator = StreamTransformer<String, String>.fromHandlers(
		handleData: (email, sink) {
			if(email.contains('@')) {
				sink.add(email);
			} else {
				sink.addError("Email invalido");
			}
		}
	);

	var passwordValidator = StreamTransformer<String, String>.fromHandlers(
		handleData: (password, sink) {
			if(password.length > 4) {
				sink.add(password);
			} else {
				sink.addError("Password Debe ser mayor a 4 caracteres");
			}
		}
	);

}