Login haciendo una validacion de los campos "Email" y "Password"
haciendo uso del patrón BloC (Bussines Logic Component o lo que es español
es Componente Logica de Negocio) haciendo alución a como se unen los widget
para crear en conjunto una Logica de Negocio que no es lo mismo que el Modelo 
de Negocio. Esta logica de negocio se refiere mas al flujo de datos de la aplicacion
y como debe ser correctamente manejada, con logica, de forma expresa y segura sin
perder de vista sacarle el jugo al rendimiento de los equipos en los que la aplicacion
funcionará.

PD: Es mi punto de vista sobre BloC que aun estoy descubriendo y entendiendo. Es lo mas
cercano a lo "entendible" luego de leer la documentacion en ingles.

Así quedaron las pantallas:

![Pagina Principal](assets/Screenshot_2019-11-27-19-39-19.png)
![Error si no se escribe el simbolo @](assets/Screenshot_2019-11-27-19-39-39.png)
![Validado y se observan los bordes en color verde si se escribe correctamente](assets/Screenshot_2019-11-27-19-39-42.png)
![Error si no se escriben mas de 4 caracteres y una alerta al usuario debajo del input](assets/Screenshot_2019-11-27-19-39-53.png)
![Password Validado/Escrito correctamente](assets/Screenshot_2019-11-27-19-39-59.png)


# login_bloc_pattern_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
